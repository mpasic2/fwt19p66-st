
 let Pretraga = (function(){
    let pretragaPredmet= function(nazivPredmeta){
       var input, filter, table, tr, td, i, txtValue;
       
      
       table = document.getElementById("tabela_sve");
       tr = table.getElementsByTagName("tr");
       input = nazivPredmeta;
       filter = input.toUpperCase();
     
       
       for (i = 0; i < tr.length; i++) {
         td = tr[i].getElementsByTagName("td")[0];
         if (td) {
           txtValue = td.textContent || td.innerText;
           if (txtValue.toUpperCase().indexOf(filter) > -1) {
             tr[i].style.display = "";
           } else {
             tr[i].style.display = "none";
           }
         }
       }
    }

    let pretragaNastavnik= function(imeNastavnika){
       var input, filter, table, tr, td, i, txtValue;
      
      
       table = document.getElementById("tabela_sve");
       tr = table.getElementsByTagName("tr");
      
       input = imeNastavnika;
       filter = input.toUpperCase();
    
       for (i = 0; i < tr.length; i++) {
         td = tr[i].getElementsByTagName("td")[2];
         if (td) {
           txtValue = td.textContent || td.innerText;
           if (txtValue.toUpperCase().indexOf(filter) > -1) {
             tr[i].style.display = "";
           } else {
             tr[i].style.display = "none";
           }
         }
       }
    }

    let pretragaGodina= function(godina){
       var input,prva;
      
       prva  = document.getElementById("tabela_sve");
       input = godina;
    
    
        if (input == 2){ 
    
          for(var i=0;i<7;i++)
          document.getElementsByTagName('tr')[i].style.display="none";
          for(var i=7;i<14;i++)
          document.getElementsByTagName('tr')[i].style.display=""; }
    
    
        else if (input == 1){
          for(var i=6;i<14;i++)
          document.getElementsByTagName('tr')[i].style.display="none"; 
          for(var i=0;i<7;i++)
          document.getElementsByTagName('tr')[i].style.display="";
        }
        
        else {
    
          for(var i=0;i<6;i++)
          document.getElementsByTagName('tr')[i].style.display="";
          for(var i=6;i<14;i++)
          document.getElementsByTagName('tr')[i].style.display=""; 
    
        }
    }

return{pretragaPredmet: pretragaPredmet, pretragaNastavnik: pretragaNastavnik, pretragaGodina: pretragaGodina}})()